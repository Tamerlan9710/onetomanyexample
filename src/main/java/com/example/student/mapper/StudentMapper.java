package com.example.student.mapper;

import com.example.student.dto.StudentRequestDto;
import com.example.student.model.Student;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "Spring", unmappedTargetPolicy = IGNORE)

public interface StudentMapper {
StudentRequestDto studentToStudentRequestDto(Student student);
Student studentRequestDtoToStudent(StudentRequestDto studentRequestDto);
List<StudentRequestDto>studentListToStudentRequestDtoList(List<Student>studentList);
List<Student>studentRequestDtoListToStudentList(List<StudentRequestDto>studentRequestDtoList);



}
