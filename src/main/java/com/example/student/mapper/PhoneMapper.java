package com.example.student.mapper;


import com.example.student.dto.PhoneRequestDto;
import com.example.student.model.Phone;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;
@Mapper(componentModel = "Spring",unmappedTargetPolicy = IGNORE)


public interface PhoneMapper {
    PhoneRequestDto phoneToPhoneRequestDto(Phone phone);
    Phone phoneRequestDtoToPhone(PhoneRequestDto phoneRequestDto);
    List<PhoneRequestDto> phoneListToPhoneRequestDtoList(List<Phone>phoneList);
    List<Phone>phooneRequestDtoListToPhoneList(List<PhoneRequestDto>phoneRequestDtoList);




}
