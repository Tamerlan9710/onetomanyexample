package com.example.student.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder

public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "phone_id")
    @JsonIgnore
    Long id;
    String provider;
    String phoneNumber;
    @ManyToOne
    @JsonIgnore

//    @JoinColumn(name = "student_id")
    Student student;
}
