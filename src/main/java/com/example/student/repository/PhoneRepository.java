package com.example.student.repository;

import com.example.student.dto.PhoneRequestDto;
import com.example.student.dto.StudentRequestDto;
import com.example.student.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface PhoneRepository extends JpaRepository<Phone, Long> {
//    Optional<Phone>findById(long id);
//    Optional<Phone>findByPhoneNumber(String phoneNumber);
}
