package com.example.student.controller;

import com.example.student.dto.StudentRequestDto;
import com.example.student.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/student/")
@RestController
@RequiredArgsConstructor

public class StudentController {
    private final StudentService studentService;

    @PostMapping("/saveStudent")
    public StudentRequestDto saveStudent(@RequestBody StudentRequestDto studentRequestDto) {

        return studentService.saveStudent(studentRequestDto);

    }

    @GetMapping("/get-by-id/{id}")
    public StudentRequestDto findStudentById (@PathVariable Long id){
        return studentService.getById(id);
    }
}