package com.example.student.controller;

import com.example.student.dto.PhoneRequestDto;
import com.example.student.service.PhoneService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/phones")
@RequiredArgsConstructor
public class PhoneController {
    private final PhoneService phoneService;

    @PostMapping("/save/{studentId}")
    public PhoneRequestDto savePhone(@PathVariable Long studentId,
                                     @RequestBody PhoneRequestDto phoneRequestDto) {
        return phoneService.savePhone(studentId, phoneRequestDto);
    }
}
