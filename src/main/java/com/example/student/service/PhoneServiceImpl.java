package com.example.student.service;

import com.example.student.dto.PhoneRequestDto;
import com.example.student.exception.PhoneNotFoundException;
import com.example.student.mapper.PhoneMapper;
import com.example.student.mapper.StudentMapper;
import com.example.student.model.Phone;
import com.example.student.model.Student;
import com.example.student.repository.PhoneRepository;
import com.example.student.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor

public class PhoneServiceImpl implements PhoneService {
    private final PhoneRepository phoneRepository;
    private final PhoneMapper phoneMapper;
    private final StudentRepository studentRepository;


    @Override
    public PhoneRequestDto savePhone(Long studentId, PhoneRequestDto phoneRequestDto) {
        Student student = studentRepository.getReferenceById(studentId);
        Phone phone = phoneMapper.phoneRequestDtoToPhone(phoneRequestDto);

        phone.setStudent(student);
        phoneRepository.save(phone);

        student.getPhones().add(phone);
        studentRepository.save(student);
        return phoneRequestDto;
    }
}












