package com.example.student.service;

import com.example.student.dto.PhoneRequestDto;

public interface PhoneService {
    PhoneRequestDto savePhone(Long studentId, PhoneRequestDto phoneRequestDto);
}
