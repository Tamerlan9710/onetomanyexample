package com.example.student.service;

import com.example.student.dto.StudentRequestDto;
import com.example.student.mapper.StudentMapper;
import com.example.student.model.Student;
import com.example.student.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    public StudentRequestDto saveStudent(StudentRequestDto studentRequestDto) {
        Student student = studentMapper.studentRequestDtoToStudent(studentRequestDto);


        return studentMapper.studentToStudentRequestDto(studentRepository.save(student));

    }

    @Override
    public StudentRequestDto getById(Long id) {
        return studentMapper.studentToStudentRequestDto(studentRepository.findById(id).get());
    }
}


















