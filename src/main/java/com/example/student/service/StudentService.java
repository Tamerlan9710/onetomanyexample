package com.example.student.service;

import com.example.student.dto.StudentRequestDto;


public interface StudentService {
    StudentRequestDto saveStudent(StudentRequestDto studentRequestDto);
    StudentRequestDto getById(Long id);

}