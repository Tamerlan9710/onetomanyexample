package com.example.student.dto;

import com.example.student.model.Student;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@Data
@Builder
@ToString
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PhoneRequestDto {

    String provider;
    String phoneNumber;
    Student student;
}
